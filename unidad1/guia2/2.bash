#!/bin/bash

clear
read -p "Introduce una palabra: " PALABRA

VOCAL=0
CONSONANTE=0

LARGO=`echo $PALABRA |wc -c`
LARGO=$(($LARGO-1))

for i in `seq 1 $LARGO`; do
    P=$(echo $PALABRA | cut -c $i)
    echo $P
    if [[ $P == "a" || $P == "A" ]]; then
        VOCAL=$((VOCAL+1))
    elif [[ $P == "e" || $P == "E" ]]; then
        VOCAL=$((VOCAL+1))
    elif [[ $P == "i" || $P == "I" ]]; then
        VOCAL=$((VOCAL+1))
    elif [[ $P == "o" || $P == "O" ]]; then
        VOCAL=$((VOCAL+1))
    elif [[ $P == "u" || $P == "U" ]]; then
        VOCAL=$((VOCAL+1))
    else
        CONSONANTE=$((CONSONANTE+1))
    fi
done

echo "La palabra $PALABRA tiene $VOCAL vocales"
echo "y $CONSONANTE consonantes"
