#!/bin/bash

PALABRA="holai"
LARGO=`echo -n $PALABRA |wc -c`

#Se reemplazan con nulos los caracteres 'aeiouAEIOU'
CP=$(echo "$PALABRA" | sed 's/[aeiouAEIOU]//g')
CONSONANTE=`echo -n $CP |wc -c`
VOCAL=$(($LARGO-$CONSONANTE))

echo "La palabra $PALABRA tiene $VOCAL vocales"
echo "y $CONSONANTE consonantes"
