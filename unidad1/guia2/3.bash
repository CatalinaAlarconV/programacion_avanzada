#!/bin/bash

function crea_ficheros(){
    touch $1
}

function random_alphanumerics(){
    #Recibe largo de caracteres a generar
    LARGO=$1
#    https://gist.github.com/earthgecko/3089509
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $LARGO | head -n 1

}

function cuenta_caracteres(){
    TOTAL=`cat $1 |wc -c`
    echo $TOTAL
}

function cuenta_vocales(){
    FICHERO=$1
    LARGO=$(cuenta_caracteres $FICHERO)
    #Se eliminan los caracteres 'aeiouAEIOU'
    CP=$(cat $FICHERO | tr -d 'aeiouAEIOU')
    CONSONANTE=`echo -n $CP |wc -c`
    VOCAL=$(($LARGO-$CONSONANTE))
    echo $VOCAL
}

function cuenta_numeros(){
    FICHERO=$1
    LARGO=$(cuenta_caracteres $FICHERO)
    #Se eliminan los caracteres '1234567890'
    CP=$(cat $FICHERO | tr -d '1234567890')
    LETRA=`echo -n $CP |wc -c`
    NUMERO=$(($LARGO-$LETRA))
    echo $NUMERO

}


function tiene_mas_caracteres(){
    #recibe archivo
    TEMP=$1
    TEMP=`cat $TEMP |sort -n -r|head -1`
    #Archivo y Valor - uso de comando cut
    ARCHIVO=`echo $TEMP |cut -d " " -f 2`
    CARACTERES=`echo $TEMP |cut -d " " -f 1`
    echo "El archivo $ARCHIVO tiene $CARACTERES"
}

function tiene_menos_caracteres(){
    #recibe archivo
    TEMP=$1
    TEMP=`cat $TEMP |sort -n |head -1`
    #Archivo y Valor - uso de comando cut
    ARCHIVO=`echo $TEMP |cut -d " " -f 2`
    CARACTERES=`echo $TEMP |cut -d " " -f 1`
    echo "El archivo $ARCHIVO tiene $CARACTERES"
}


#MAIN
if mkdir ejercicio3 2>/dev/null; then
    echo "Directorio creado"
    cd ejercicio3
else
    echo "El directorio ya existe"
    cd ejercicio3
    rm -f *
fi

for i in `seq 1 3` ; do
    NOMBRE=$(random_alphanumerics 3)
    crea_ficheros $NOMBRE
done

#Crea archivo temporal
rm temp 2>/dev/null
crea_ficheros temp

for i in `ls -I temp`; do
    NUMERO=$RANDOM
    random_alphanumerics $NUMERO > $i
    
    #duplicidad intencional por propuesta de ejemplo,
    #Es posible y evidente que se puede escribir el valor
    #RANDOM directamente archivo temporal.
    echo $(cuenta_caracteres $i) $i >> temp
done

cat temp
echo -n "Tiene más caracteres: "
tiene_mas_caracteres temp 

cat /dev/null > temp

for i in `ls -I temp`; do
    echo $(cuenta_vocales $i) $i >> temp
done

#cat temp
echo -n "Tiene más vocales: "
tiene_mas_caracteres temp

cat /dev/null > temp
for i in `ls -I temp`; do
    echo $(cuenta_numeros $i) $i >> temp
done

cat temp
echo -n "Tiene más números: "
tiene_mas_caracteres temp
echo -n "Tiene menos números: "
tiene_menos_caracteres temp

