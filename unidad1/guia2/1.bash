#!/bin/bash

without_param () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || without_param "Un parámetro es requerido, $# provided"

echo $1 | grep -E -q '^[0-9]+$' || without_param "Un parámetro númerico es requerido, $1 no es un número"

NUMERO=$1

if [[ $NUMERO -lt 100 || $NUMERO -gt 999 ]]; then
    echo "El numero no tiene 3 cifras"
else
    CIFRA1=$(echo $NUMERO | cut -c 1)
    CIFRA3=$(echo $NUMERO | cut -c 3)
    if [ $CIFRA1 -eq $CIFRA3 ]; then
        echo "El número es capicúa"
    else
        echo "El número no es capicúa"
    fi
fi
