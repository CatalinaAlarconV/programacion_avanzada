#!/bin/bash

PALABRA="holai"
LARGO=`echo -n $PALABRA |wc -c`

#Se eliminan los caracteres 'aeiouAEIOU'
CP=$(echo "$PALABRA" | tr -d 'aeiouAEIOU')
CONSONANTE=`echo -n $CP |wc -c`
VOCAL=$(($LARGO-$CONSONANTE))

echo "La palabra $PALABRA tiene $VOCAL vocales"
echo "y $CONSONANTE consonantes"
