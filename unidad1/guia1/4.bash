#!/bin/bash

FECHA=`date +%Y%m%d`
mkdir $FECHA 2>>error.txt
cd $FECHA

for i in `seq 1 40`; do
    touch $i
done

for i in `ls -v`; do
    if [[ $i = 1 ]]; then
        x=$i
    else
        x="$x,$i"
    fi
    echo $x >> $i
done

if tar -cZ3f uno.tar {11..20} ; then
    echo "uno.tar ha sido generado"
else
    echo "Algo salió mal"
fi

if zip dos.zip {25..29}; then
    echo "dos.zip ha sido creado"
else
    echo "Algo salió mal"
fi

