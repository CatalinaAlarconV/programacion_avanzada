#!/bin/bash

FILE="$HOME/ejercicio3.txt"

cd /bin
echo "Usted se encuentra en el directorio `pwd`" > $FILE
echo "Todos los archivos en el directorio `pwd` que inician con la letra c" >> $FILE
ls c* 1>> $FILE 

echo "" >> $FILE
echo "Todos los archivos en el directorio `pwd` que terminan con la letra t" >> $FILE
ls *t 1>> $FILE

echo "" >> $FILE
echo "Todos los archivos en `pwd` que inician con c terminan con t" >> $FILE
ls c*t 1>> $FILE

echo "" >> $FILE
echo "Muestra resultados en `pwd` que solo contienen 3 caracteres en `pwd`" >> $FILE
ls ??? 1>> $FILE

echo "" >> $FILE
echo "Muestra todos los resultados en `pwd` que terminan en l o u" >> $FILE
ls *[l,u] 1>> $FILE

echo "" >> $FILE
echo "Muestra todos los resultados en `pwd` que comiencen con b y terminan en l o t" >> $FILE
ls b*[l,t] 1>> $FILE

cat $FILE
