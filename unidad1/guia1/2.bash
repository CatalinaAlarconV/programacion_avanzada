#!/bin/bash

echo "Presione una tecla"
read KEY

if [[ $KEY = [a-z,A-Z] ]]; then
    echo "$KEY es una letra"
elif [[ $KEY = [0-9] ]]; then
    echo "$KEY es un número"
else
    echo "$KEY es un caracter especial"
fi
