#!/bin/bash

OPT=""
NUM1=6
NUM2=3

while [[ $OPT != "s" ]]; do

    echo "Presione 1 para SUMAR $NUM1 y $NUM2"
    echo "Presione 2 para RESTAR $NUM1 y $NUM2"
    echo "Presione 3 para MULTIPLICAR $NUM1 y $NUM2"
    echo "Presione 4 para DIVIDIR  $NUM1 y $NUM2"
    echo "Si desea finalizar el programa presione s/n"
    read OPT
    clear
    if [[ $OPT -eq 1 ]]; then
        echo "La suma de $NUM1 y $NUM2 es: " $(($NUM1+$NUM2))
    elif [[ $OPT -eq 2 ]]; then 
        echo "La resta de $NUM1 y $NUM2 es; "$(($NUM1-$NUM2))
    elif [[ $OPT -eq 3 ]]; then
        echo "La multiplicación de $NUM1 y $NUM2 es "$(($NUM1*$NUM2))
    elif [[ $OPT -eq 4 ]]; then
        echo "La División entre $NUM1 y $NUM2 es "$(($NUM1/$NUM2))
    elif [[ $OPT -eq "s" ]]; then
        echo "Ha finalizado el programa"
    else
        echo "Opción invalida"
    fi
    echo ""
    echo ""
    echo ""
done;
