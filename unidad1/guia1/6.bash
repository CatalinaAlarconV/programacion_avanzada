#!/bin/bash


HORA=`date +%_I`
echo "La hora actual es: "$HORA

echo "Indique una cantidad de horas a sumar: "
read ENTRADA

for i in `seq 1 $ENTRADA`; do
    if [[ $HORA -eq 12 ]]; then
        HORA=0
    fi
    HORA=$(($HORA+1))

done
echo $HORA

