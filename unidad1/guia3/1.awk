# esto se ejecutará solo una vez al principio
BEGIN {  
  print "--------------------------------------"
  print "| NOMBRE    P1 P2 P3 | PROM | APROBO |"  
  print "--------------------------------------"
  }

# esto se ejecutará para cada una de las líneas del fichero
{

    suma2+=$2
    suma3+=$3
    suma4+=$4
    mediaFila=($2+$3+$4)/3
    
    aprobo="NO"
    if ( mediaFila >= 40 ) {
        aprobo="SI"    
        aprobados++  
    }
    
    print "| "$0" | "mediaFila" | "aprobo"  |"
   
}

# esto se ejecutará solo una vez al final
END {  
    p2=suma2/3
    p3=suma3/3  
    p4=suma4/3  
    promedio=(p2+p3+p4)/3  
    print "----------------------------------"
    print "| PROMEDIO  "p2 p3 p4"| "promedio" |  "aprobados"  |"  
    print "----------------------------------"
}
