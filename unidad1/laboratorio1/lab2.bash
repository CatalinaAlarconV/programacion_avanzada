#!/bin/bash

# Evalúa parámetros
if [ "$#" == "0" ]; then
    # Mensaje de error
    echo "La forma de uso es la siguiente bash lab2.sh directorio"
    exit 1
fi

DIRECTORIO=$1

if [[ -d $DIRECTORIO ]]; then
    echo "El directorio $DIRECTORIO ya existe"
    echo -n "Ingrese un nuevo nombre para el directorio: "
    read DIRECTORIO
    mkdir $DIRECTORIO
else
    mkdir $DIRECTORIO
fi

cd $DIRECTORIO

for i in `cat ../temp`; do
    touch $i
done

for i in `ls`; do
    # Se extrae el primer caracter de cada archivo
    VAR=`echo $i |awk '{print substr($1, 1, 1)}'`
    # Crea directorio, en caso de error omite
    # El error, existe el mismo directorio
    mkdir $VAR 2>/dev/null
    # Mueve los ficheros al directorio creado
    mv $i $VAR
done
