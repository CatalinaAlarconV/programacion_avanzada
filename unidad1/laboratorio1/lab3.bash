#! /bin/bash

# si el número de parámetros es igual a 0
if [ "$#" == "0" ]; then
# muestra el calendario del mes actual y sale
    cal
    exit 0
fi
# si el número de parámetros es distinto de 1
if [ "$#" != "1" ]; then
# muestra un mensaje de error y sale
    echo "Sólo se admite un parámetro."
    exit 1
fi
# dependiendo del parámetro introducido

if [[ $1 == '-s' || $1 == '--short' ]]; then
    date +"%d/%m/%Y"
elif [[ $1 == '-l' || $1 == '--long' ]]; then
    date +"Hoy es el día '%d' del mes '%m' del año '%Y'." 
else
    echo "Opción incorrecta, solo se acepta el parámetro -c o -l"
    exit 1
fi

exit 0
