#!/bin/bash

# Evalúa parámetros
if [ "$#" == "0" ]; then

    # Mensaje de error
    echo "Se requiere de un directorio a listar"
    echo "La forma de uso es la siguiente bash ordena.sh directorio"
    exit 1
fi

# Main
DIRECTORIO=$1
# Opción 1
echo "Opción 1"
ls -ll $DIRECTORIO |tail -2 |sort -nk 5 | awk '{ print NR " " $9 }'

# Opción 2
echo "Opción 2"
ls -Sr $DIRECTORIO | awk '{ print NR " " $1 }'

# Opción 3 (Mejor opción)
echo "Opción 3"
ls -Sr  $DIRECTORIO |awk '{ printf "%s", NR " " $1; $1=""; print $0 }'
