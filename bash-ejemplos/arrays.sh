#!/bin/bash

AR=(1 2 3)
echo "Forma incorrecta"
echo $AR

echo "Forma correcta"
echo ${AR[*]}

echo "Imprime posicion indice 2"
echo ${AR[2]}

echo "agrega valor posicion 3"
AR[3]=4
echo ${AR[*]}

echo "Elimina elemento indice 1"
unset AR[1]
echo ${AR[*]}
