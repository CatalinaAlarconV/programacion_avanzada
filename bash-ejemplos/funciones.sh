#!/bin/bash

function salir {
  exit
}

function hola {
  echo Hola Mundo!
}

function hola-con-parametros {
echo "$1 $2"
}

echo $1
hola
hola-con-parametros Hola Mundo
salir
