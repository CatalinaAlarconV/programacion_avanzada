#!/bin/bash

function ejemplo() {
  local TEXT='Hola Mundo'
  echo $TEXT
}

RESULTADO="$(ejemplo)"
echo $RESULTADO


function ejemplo2(){
    # return 0
    return 1
}

if ejemplo2; then
    echo "Verdadero"
else
    echo "Falso"
fi
