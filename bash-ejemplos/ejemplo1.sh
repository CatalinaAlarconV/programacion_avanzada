#!/bin/bash
clear; 
echo "Información dada por mi shell script"
echo "Hola, $USER"
echo
echo "La fecha es `date`, y esta semana `date +"%V"`."
echo
echo "Usuarios conectados:"
w | cut -d " " -f 1 - | grep -v USER | sort -u
echo
echo "El sistema es `uname -s` y el procesador es `uname -m`."
echo
echo "El sistema está encendido desde hace:"
uptime
echo
echo "FIN"
