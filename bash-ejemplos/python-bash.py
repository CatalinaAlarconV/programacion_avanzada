#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess


class Test():

    def __init__(self):
        result = subprocess.Popen('date', stdout=subprocess.PIPE, shell=True)
        (output, err) = result.communicate()
        p_status = result.wait()
        print ("Command output : ", str(output))
        print ("Command exit status/return code : ", p_status)
        
        print(output)
        
        texto = ''
        for i in output.decode("utf-8"):
            texto += i

        print(texto)
        self.save_file(texto, 'test.txt')
        
        
    def save_file(self, txt, filename):
        with open(filename, 'w') as file:
             file.writelines(txt)

if __name__ == '__main__':
    Test()
