#!/bin/bash

wget http://icb.utalca.cl/docs/sequence.fasta
wget http://icb.utalca.cl/docs/sequence1.fasta

mkdir files
cat sequence.fasta sequence1.fasta > combine.fasta

for i in AAAA TTT GGG CCCC; do
    cat combine.fasta | grep $i > $i.fasta
done

for i in AAAA TTT GGG CCCC; do
    mv $i.fasta files
done

exit
