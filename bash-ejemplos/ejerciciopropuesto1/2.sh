#!/bin/bash

NEWFOLDER=$USER`date +%Y%m%d`

if [[ ! -d $NEWFOLDER ]]; then
    mkdir $NEWFOLDER
fi

cd $NEWFOLDER
NEWFILE="newfile3"

if [[ ! -f $NEWFILE ]]; then
    touch $NEWFILE
else
    cat /dev/null > $NEWFILE
fi

MAX=$RANDOM
echo "El número RANDOM es "$MAX

for i in `seq 1 $MAX`; do
    echo $MAX 1>> $NEWFILE
    MAX=$((MAX-1))
done
