#!/bin/bash

N=6 

# Primer numero de la serie
a=0 
# Segundo numero de la serie
b=1 

echo "La serie Fibonacci : "

for i in `seq 1 $N`; do
#for (( i=0; i<N; i++ )); do
	echo -n "$a "
	fn=$((a + b)) 
	a=$b 
	b=$fn 
done
echo
# End of for loop 

