#!/bin/bash

NUM1=1
NUM2=11
NUM3=11

if [[ $NUM2 -eq $NUM3 ]]; then
    echo "NUM2 es igual a NUM3"
fi

if [[ $NUM1 -ne $NUM2 ]]; then
    echo "NUM1 no es igual a NUM2"
fi

if [[ $NUM1 -lt $NUM2 ]]; then
    echo "NUM1 es menor a NUM2"
fi

if  [[ $NUM1 -lt 2  && $NUM3 -gt 5 ]]; then
    echo "NUM1 es menor a 2 y NUM3 es mayor a 5"
fi

STRING="HOLA MUNDO"
if  [[ $STRING =~ ^"HOLA1" ]]; then
    echo "STRING Comienza por HO"
    
elif [[ $STRING =~ MUNDO$ ]]; then
  echo "Termina en MUNDO"
  
else 
    echo "NO coincide"
fi
